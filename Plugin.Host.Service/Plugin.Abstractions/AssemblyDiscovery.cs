﻿using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Plugin.Abstractions
{
    public static class AssemblyDiscovery
    {
        public static IServiceCollection RegisterAutoDiscovery(this IServiceCollection services, Assembly asm)
        {
            var types = asm
                .GetTypes()
                .Where(x => x.CustomAttributes.Any(x =>
                        x.GetType() == typeof(ServiceCollectionDiscoveryAttribute)));

            var methodsToRegister = types
                .Select(x => x.GetMethods().Where(x => x.IsPublic && x.IsStatic))
                .SelectMany(x => x);

            foreach (var method in methodsToRegister)
            {
                _ = method.Invoke(null, [services]);
            }

            return services;
        }
    }
}
