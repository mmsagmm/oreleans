﻿namespace Plugin.Abstractions
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ServiceCollectionDiscoveryAttribute : Attribute
    {

    }
}
