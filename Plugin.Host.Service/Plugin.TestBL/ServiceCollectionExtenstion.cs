﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Plugin.Abstractions;
using System.Reflection;

namespace Plugin.TestBL
{
    [ServiceCollectionDiscovery]
    public static class ServiceCollectionExtenstion
    {
        //Adding of controller could be moved to packages with abstraction
        public static IServiceCollection AddControllers(IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var controllers = assembly.DefinedTypes.Where(x => x.BaseType == typeof(ControllerBase) && !x.IsAbstract);
            foreach (var controller in controllers)
            {
                services.AddTransient(controller);
            }

            return services;
        }
    }
}
