﻿using Microsoft.AspNetCore.Mvc;

namespace Plugin.TestBL
{
    [Route("handler/v2")]
    public class HandlerController : ControllerBase
    {
        private string _state;

        [HttpGet("state")]
        public IActionResult GetState(string id)
        {
            return Ok(_state);
        }

        [HttpPost("init")]
        public IActionResult InitState(string id)
        {
            _state = Guid.NewGuid().ToString();
            return Ok(_state);
        }
    }
}
