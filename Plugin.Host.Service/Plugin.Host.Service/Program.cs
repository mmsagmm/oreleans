using Plugin.TestBL;
using static Plugin.Abstractions.AssemblyDiscovery;

var builder = WebApplication.CreateBuilder(args);

//Need to load on fly typeof(HandlerController).Assembly from config service
builder.Services.RegisterAutoDiscovery(typeof(HandlerController).Assembly);

builder
    .Services
    .AddSwaggerGen()
    .AddMvcCore()
    .AddApiExplorer()
    .AddControllersAsServices();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}

app.MapControllers();
app.UseSwagger();
app.UseSwaggerUI();


app.UseRouting();

app.Run();
