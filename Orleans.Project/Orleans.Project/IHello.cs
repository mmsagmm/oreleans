﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orleans.Project
{
    [Alias("Orleans.Project.IHello")]
    public interface IHello: IGrainWithIntegerKey
    {
        Task<string> SayHello(string greeting);
    }
}
