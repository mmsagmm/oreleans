﻿namespace Orleans.Project
{
    public class HelloGrain : Grain, IHello
    {
        public Task<string> SayHello(string greeting)
        {
            return Task.FromResult($"You said {greeting}");
        }
    }
}
